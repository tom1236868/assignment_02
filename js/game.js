var Game = {
    preload: function(){
        this.bgmplayer = game.add.audio('game');
        this.bgmplayer.loop = true;
        this.bgmplayer.volume = game.global.bgmvalue*0.1;

        this.bgmplayer.fadeIn();

        this.laser = game.add.audio('shoot');
        this.laser.loop = false;
        this.laser.volume = game.global.bgmvalue*0.1;

        this.boom = game.add.audio('boom');
        this.boom.loop = false;
        this.boom.volume = game.global.bgmvalue*0.1;
    },
    create: function(){
        ///variable///
        this.cursor = game.input.keyboard.createCursorKeys();
        this.btn_fire = fireButton = game.input.keyboard.addKey(Phaser.KeyCode.Z);
        this.btn_ult = fireButton = game.input.keyboard.addKey(Phaser.KeyCode.X);
        this.btn_pause = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.btn_pause.onDown.add(this.Pause, this, 0);


        this.killCounts = 0;
        this.enemytotal = 0;
        this.ultCount = 3;
        game.global.difficulty = 1;
        ///player setting///
        this.player = game.add.sprite(game.width/2, game.height-50, 'player');
        this.player.skew = 0; //0:default , 1:right , 2: left
        this.player.health = 3;
        this.player.frame = 5;
        this.player.type = 0;
        ///player UI///
        this.heart = game.add.sprite(0, game.height-150, 'heart');
        this.heart.anchor.setTo(0, 0.5);
        this.heart.width = 30;
        this.heart.height = 30;
        this.healthBar = game.add.sprite(0, game.height-130, 'health');
        this.healthBar.anchor.setTo(0, 0);
        this.healthBar.width = 30;
        this.healthBar.height = 90;
        this.killcount_tag = game.add.text(50, 50, 'Kill: ' + this.killCounts, {font:'30px Arial', fill:'#000000'});
        this.killcount_tag.anchor.setTo(0.5, 0.5);
        this.ultcount_tag = game.add.text(550, 50, 'Ult: ' + this.ultCount, {font:'30px Arial', fill:'#000000'});
        this.ultcount_tag.anchor.setTo(0.5, 0.5);
        ///player animation///
        this.player.animations.add('0to1', [5,6,7,8,9,10], 30, false);
        this.player.animations.add('0to2', [5,4,3,2,1,0], 30, false);
        this.player.animations.add('1to2', [10,9,8,7,6,5,4,3,2,1,0], 30, false);
        this.player.animations.add('2to1', [0,1,2,3,4,5,6,7,8,9,10], 30, false);
        this.player.animations.add('1to0', [10,9,8,7,6,5], 30, false);
        this.player.animations.add('2to0', [0,1,2,3,4,5], 30, false);
        ///weapon setting///
        this.player.weapon = game.add.weapon(40, 'bullet');
        this.player.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.player.weapon.bulletAngleOffset = 90;
        this.player.weapon.bulletSpeed = 400;
        this.player.weapon.fireRate = 150;
        this.player.weapon.trackSprite(this.player, this.player.width/2, 0);
        this.player.weapon.addBulletAnimation('fly', [0,1], 30, true)
        ///emitter///
        this.emitter = game.add.emitter(0,0,15);
        this.emitter.makeParticles('pixel', [0,1,2,3]);
        this.emitter.setXSpeed(-100, 100);
        this.emitter.setYSpeed(0, 400);
        this.emitter.setScale(0.25, 0.25, 0.25, 0.25, 60)
        this.emitter.gravity = 0;
        ///enemy///
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;    

        game.time.events.loop(1500, this.SpawnEnemy ,this)

        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;
       
        


    },
    update: function(){
        this.PlayerContorl();
        this.enemies.forEachAlive(this.UpadteEnemy, this);
        this.healthBar.height = 30 * this.player.health;
        this.ultcount_tag.text = 'Ult: ' + this.ultCount;
        this.killcount_tag.text = 'Kill: ' + this.killCounts;
        if(this.killCounts >= game.global.killRecord)
                {
                    game.global.killRecord = this.killCounts;
                    console.log('new record!');
                }
        this.bgmplayer.volume = game.global.bgmvalue*0.1;
        this.laser.volume = game.global.bgmvalue*0.1;
        this.boom.volume = game.global.bgmvalue*0.1;
        game.physics.arcade.overlap(this.enemies, this.player.weapon.bullets, this.BulletHit,null, this);
        game.physics.arcade.collide(this.player, this.enemies, this.BulletHit);
        game.global.difficulty = this.killCounts/10 + 1;
    },
    PlayerContorl: function(){
        if(this.cursor.left.isDown){
            this.player.body.velocity.x = -200;
            if(this.player.frame == 5)
                this.player.animations.play('0to2');
            else if(this.player.frame >= 6)
                this.player.animations.play('1to0');
            this.player.skew = 2;
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 200;
            if(this.player.frame == 5)
                this.player.animations.play('0to1');
            else if(this.player.frame <= 4)
                this.player.animations.play('2to0');
            this.player.skew = 1;
        }
        else {
            this.player.body.velocity.x = 0;
            if(this.player.frame != 5)
            {
                if(this.player.frame >= 6) {
                    this.player.animations.play('1to0');
                    if(this.player.animations.getAnimation('0to1').isPlaying)
                        this.player.animations.getAnimation('1to0').setFrame(this.player.animations.getAnimation('0to1').currentFrame.index);
                }else if(this.player.frame <= 4){
                    this.player.animations.play('2to0')
                    if(this.player.animations.getAnimation('0to2').isPlaying)
                        this.player.animations.getAnimation('2to0').setFrame(this.player.animations.getAnimation('0to2').currentFrame.index);
                }
            }
            else
            {
                this.player.skew = 0;
                this.player.frame = 5;
            }
                
        }

        if(this.cursor.up.isDown){
            this.player.body.velocity.y = -200;
        }
        else if(this.cursor.down.isDown){
            this.player.body.velocity.y = 200;
        }
        else
        {
            this.player.body.velocity.y = 0;
        }

        if (this.btn_fire.isDown)
        {
            this.player.weapon.fire();
            this.emitter.x = this.player.x + this.player.width/2;
            this.emitter.y = this.player.y;
            this.emitter.start(true, 150, null, 15);
            this.laser.play();
        }

        this.btn_ult.onDown.add(this.Ultimate, this, 0)

    },
    SpawnEnemy: function(){
        var enemy;
        var prob = [1,1,1,2,3];

        if(!this.enemies.getFirstDead(true))
            return;

        if(game.global.difficulty >= 3)
        {
            for(i=0; i<game.global.difficulty-2; i++)
                prob.push(2);
            for(j=0; j<game.global.difficulty-3; j+=2)
                prob.push(3);
        }

        enemytype = game.rnd.pick(prob);
        respawnPointX = game.rnd.integerInRange(30, 570);
        respawnPointY = game.rnd.integerInRange(-20, 0)

        if(game.global.difficulty >= 1){
            if(enemytype == 1)
            {
                enemy = game.add.sprite(-100, -100, 'enemy1');
                game.physics.arcade.enable(enemy);
                enemy.anchor.setTo(0.5, 1)
                enemy.reset(respawnPointX,respawnPointY);
                enemy.body.velocity.y = 50;
                enemy.outOfBoundsKill = true;
                enemy.type = 1;
                enemy.health = 2;
                ///weapon///
                enemy.weapon = game.add.weapon(40, 'bullet');
                enemy.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
                enemy.weapon.bulletAngleOffset = -90;
                enemy.weapon.bulletSpeed = -200;
                enemy.weapon.fireRate = 1000;
                enemy.weapon.trackSprite(enemy, 0, 0);
                enemy.weapon.addBulletAnimation('fly', [0,1], 30, true);
            }
            else if(game.global.difficulty >= 1 && enemytype == 2)
            {
                enemy = game.add.sprite(-100, -100, 'enemy2');
                game.physics.arcade.enable(enemy);
                enemy.anchor.setTo(0.5, 1)
                enemy.reset(respawnPointX,respawnPointY);
                enemy.body.velocity.y = 100;
                enemy.outOfBoundsKill = true;
                enemy.type = 2;
                enemy.health = 4;
                ///weapon///
                enemy.weapon = game.add.weapon(40, 'bullet');
                enemy.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
                enemy.weapon.bulletAngleOffset = 90;
                enemy.weapon.bulletSpeed = 300;
                enemy.weapon.fireRate = 900;
                enemy.weapon.trackSprite(enemy, 0, 0, true);
                enemy.weapon.addBulletAnimation('fly', [0,1], 30, true);
            }
            else if(game.global.difficulty >= 3 && enemytype == 3)
            {
                enemy = game.add.sprite(-100, -100, 'enemy3');
                game.physics.arcade.enable(enemy);
                enemy.anchor.setTo(0.5, 1)
                enemy.reset(respawnPointX,respawnPointY);
                enemy.body.velocity.y = 150;
                enemy.outOfBoundsKill = true;
                enemy.type = 3;
                enemy.health = 35;
                ///weapon///
                enemy.weapon = game.add.weapon(40, 'bullet');
                enemy.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
                enemy.weapon.bulletSpeed = 400;
                enemy.weapon.fireRate = 400;
                enemy.weapon.trackSprite(enemy, 0, 0, true);
                enemy.weapon.addBulletAnimation('fly', [0,1], 30, true);
            }
            else{
                enemy = game.add.sprite(-100, -100, 'enemy1');
                game.physics.arcade.enable(enemy);
                enemy.anchor.setTo(0.5, 1)
                enemy.reset(respawnPointX,respawnPointY);
                enemy.body.velocity.y = 50;
                enemy.outOfBoundsKill = true;
                enemy.type = 1;
                enemy.health = 2;
                ///weapon///
                enemy.weapon = game.add.weapon(40, 'bullet');
                enemy.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
                enemy.weapon.bulletAngleOffset = -90;
                enemy.weapon.bulletSpeed = -200;
                enemy.weapon.fireRate = 1000;
                enemy.weapon.trackSprite(enemy, 0, 0);
                enemy.weapon.addBulletAnimation('fly', [0,1], 30, true)
            }
        }
        this.enemytotal += 1;
        this.enemies.add(enemy)
    },
    BulletHit: function(target, bullet){
        bullet.kill();

        target.health -= 1;

        if(target.health < 1)
        {
            if(target.type == 0)
            {
                game.sound.stopAll();
                
                   
                game.state.start('menu');
            }
            else
            {
                target.kill();
                this.killCounts += 1;
            }
                
        }

    },
    UpadteEnemy: function(enemy){
        switch(enemy.type){
            case 1:
                enemy.weapon.fire();
                break;
            case 2:
                enemy.rotation = game.physics.arcade.angleToXY(enemy, this.player.x+this.player.width/2, this.player.y+this.player.height/2)-1.57;
                enemy.weapon.fireAtSprite(this.player);
                break;
            default:
        }
    },
    Ultimate: function(){
        if(this.ultCount > 0)
        {
            this.ultCount -= 1;
            this.killCounts += this.enemies.countLiving();
            this.enemies.forEachAlive(function(enemy){
                enemy.kill();
            });
            this.boom.play();
            game.camera.flash(0xffffff, 300);
            game.camera.shake(0.02, 300);
        }
    },
    Pause: function(){
        if(!game.paused)
            game.paused = true;
        else
            game.paused = false;
    }
};




