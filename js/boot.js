var Boot = {
    preload: function(){
        game.load.spritesheet('loader', 'Assets/load.png', 282, 442);
    },
    create: function(){
        game.stage.backgroundColor = '#eeccff';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        game.state.start('load');

    },
    update: function(){

    }
};



