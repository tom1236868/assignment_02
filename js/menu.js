var Menu = {
    preload: function(){
        
    },
    create: function(){

        this.bgmplayer = game.add.audio('bgm');
        this.bgmplayer.loop = true;
        this.bgmplayer.volume = game.global.bgmvalue*0.1;

        this.bgmplayer.play();

        this.sfxplayer = game.add.audio('button');
        this.bgmplayer.loop = false;
        this.bgmplayer.volume = game.global.sfxvalue*0.1;

        ///bgm///
        this.bgmvalue_tag = game.add.text(game.width/2-150, game.height/2, 'BGM: ', {font:'30px Arial', fill:'#000000'});
        this.bgmvalue_tag.anchor.setTo(0.5, 0.5);
        this.bgmvalue = game.add.text(game.width/2, game.height/2, game.global.bgmvalue, {font:'30px Arial', fill:'#000000'});
        this.bgmvalue.anchor.setTo(0.5, 0.5);

        this.btn_bgmUp = game.add.button(game.width/2 + 50, game.height/2, 'button_right', this.bgmUp, this);
        this.btn_bgmUp.anchor.setTo(0, 0.5);
        this.btn_bgmUp.width = 50;
        this.btn_bgmUp.height = 50;

        this.btn_bgmDown = game.add.button(game.width/2 - 50, game.height/2, 'button_left', this.bgmDown, this);
        this.btn_bgmDown.anchor.setTo(1, 0.5);
        this.btn_bgmDown.width = 50;
        this.btn_bgmDown.height = 50;
        ///sfx///
        this.sfxvalue_tag = game.add.text(game.width/2-150, game.height/2+70, 'SFX: ', {font:'30px Arial', fill:'#000000'});
        this.sfxvalue_tag.anchor.setTo(0.5, 0.5);
        this.sfxvalue = game.add.text(game.width/2, game.height/2+70, game.global.sfxvalue, {font:'30px Arial', fill:'#000000'});
        this.sfxvalue.anchor.setTo(0.5, 0.5);

        this.btn_sfxUp = game.add.button(game.width/2 + 50, game.height/2+70, 'button_right', this.sfxUp, this);
        this.btn_sfxUp.anchor.setTo(0, 0.5);
        this.btn_sfxUp.width = 50;
        this.btn_sfxUp.height = 50;

        this.btn_sfxDown = game.add.button(game.width/2 - 50, game.height/2+70, 'button_left', this.sfxDown, this);
        this.btn_sfxDown.anchor.setTo(1, 0.5);
        this.btn_sfxDown.width = 50;
        this.btn_sfxDown.height = 50;
        ///start///
        this.btn_start = game.add.button(game.width/2, 200, 'button', this.start, this);
        this.btn_start.anchor.setTo(0.5, 0.5);
        this.btn_start.width = 200;
        this.btn_start.height = 80;

        this.starttext = game.add.text(game.width/2, 200, 'Start game', {font:'30px Arial', fill:'#000000'});
        this.starttext.anchor.setTo(0.5, 0.5);

        this.highscore = game.add.text(game.width/2, 500, 'Highest score: ' + game.global.killRecord, {font:'30px Arial', fill:'#000000'});

    },
    update: function(){
        this.sfxplayer.volume = game.global.sfxvalue*0.1;
        this.bgmplayer.volume = game.global.bgmvalue*0.1;
    },
    bgmUp: function(){
        if(game.global.bgmvalue < 10)
            game.global.bgmvalue += 1;
        this.bgmvalue.text = game.global.bgmvalue;
        this.bgmplayer.volume = game.global.bgmvalue*0.1;
        this.sfxplayer.play();
    },
    bgmDown: function(){
        if(game.global.bgmvalue > 0)
            game.global.bgmvalue -= 1;
        this.bgmvalue.text = game.global.bgmvalue;
        this.bgmplayer.volume = game.global.bgmvalue*0.1;
        this.sfxplayer.play();
    },
    sfxUp: function(){
        if(game.global.sfxvalue < 10)
            game.global.sfxvalue += 1;
        this.sfxvalue.text = game.global.sfxvalue;
        this.sfxplayer.volume = game.global.sfxvalue*0.1;
        this.sfxplayer.play();
    },
    sfxDown: function(){
        if(game.global.sfxvalue > 0)
            game.global.sfxvalue -= 1;
        this.sfxvalue.text = game.global.sfxvalue;
        this.sfxplayer.volume = game.global.sfxvalue*0.1;
        this.sfxplayer.play();
    },
    start: function(){ 
        this.bgmplayer.fadeOut();
        if(this.bgmplayer.onFadeComplete)
        {
            this.bgmplayer.stop();
            game.state.start('game');
        }
            
    }
};