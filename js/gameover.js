var GameOver = {
    preload: function(){
        
    },
    create: function(){


    },
    update: function(){

    }
};
var game = new Phaser.Game(600, 600, Phaser.AUTO, 'canvas');

game.global = {
    bgmvalue: 10,
    sfxvalue: 10,
    killRecord: 0,
    difficulty: 1
}

game.state.add('boot', Boot);
game.state.add('load', Load);
game.state.add('menu', Menu);
game.state.add('game', Game);
game.state.start('boot');

