var Load = {
    preload: function(){
        this.loader = game.add.sprite(game.width/2, 300, 'loader');
        this.loader.anchor.setTo(0.5,0.5);

        this.loader.animations.add('load', null, 30, true);

        var loadtext = game.add.text(game.width/2, 550, 'Loading...', {font:'30px Arial', fill:'#000000'});
        loadtext.anchor.setTo(0.5, 0.5);

        this.loader.animations.play('load');

        //////////////Load Assets Below//////////////

        game.load.image('button', 'Assets/cga_ui_gold/b_2.png');
        game.load.image('button_right', 'Assets/cga_ui_gold/b_7.png');
        game.load.image('button_left', 'Assets/cga_ui_gold/b_6.png');
        game.load.image('textarea', 'Assets/cga_ui_gold/b_5.png');
        game.load.image('enemy1', 'Assets/enemy1.png');
        game.load.image('enemy2', 'Assets/enemy2.png');
        game.load.image('heart', 'Assets/cga_ui_gold/l1.png');
        game.load.image('health', 'Assets/health.png');

        game.load.spritesheet('player', 'Assets/Player.png', 34, 38);
        game.load.spritesheet('bullet', 'Assets/bullet.png', 18, 19);
        game.load.spritesheet('pixel', 'Assets/pixel.png', 8, 8);

        game.load.audio('bgm', 'Assets/bgm.mp3');
        game.load.audio('boom', 'Assets/explosion.mp3');
        game.load.audio('shoot', 'Assets/shoot.mp3');
        game.load.audio('game', 'Assets/game.mp3');
        game.load.audio('button', 'Assets/button.mp3');


    },
    create: function(){
        
    },
    update: function(){
        if(game.cache.isSoundReady('bgm') && game.cache.isSoundReady('boom') && game.cache.isSoundReady('shoot') && game.cache.isSoundReady('button') && game.cache.isSoundReady('game')){
            game.state.start('menu');
        }
    }
};