# SS Assignment 02 Raiden

## Jucify mechanisms
* Key functions
    1. 簡單的難度設計，難度隨著殺敵數而增加，增加的方式包括新增敵人，和高等級敵人出現機率變高。
    2. 玩家的大絕搭配簡單的動畫和音效，可以清場，只能清掉敵機，不能清掉子彈。
    
* Other functions
    1. 音效中控，即使切換state，音效也是統一大小。
    2. 敵人及玩家都有血量設定，增加難度。
    3. 較高級的敵人會根據玩家的位置發射子彈。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms(describe on README.md)|15%|Y|
|Animations / Particle Systems|20%|Y/Y|
|UI / Sound effects /Leaderboard|15%|Y/Y/N|

## Bonus
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi player game|15+5%|N/N|
|Bullet automatic aiming|5%|N|
|Unique bullet|2.5%|N|
|Little helper|5%|N|
|Unique movement and attack mode|5%|N|

# 作品網址：https://106000115.gitlab.io/assignment_02

